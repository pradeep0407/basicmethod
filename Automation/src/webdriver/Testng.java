package webdriver;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

@Listeners(webdriver.Liserner.class)
 public class Testng extends Liserner{
	 
       public static void main(String[] args) {
    	   Testng  tng=new Testng (); 
    	  // tng.chrome();
}
	@BeforeTest 
	public static void method1() {
       System.out.println("method1");
	}
	@AfterTest
	public static void method2() {
	     System.out.println("method2");
	}
	@BeforeTest
	public static void method3() {
	     System.out.println("method3");
	}
	@AfterTest
	public static void method() {
	     System.out.println("method");
	}
	
	@Test(parameters = {"xyz"})
	public static void method100() {
	     System.out.println("it is run by parameter");
	}
	@Test(priority=2)
	public static void method200() {
	     System.out.println("method200");
	}
	
	@Test(parameters = {"Browsername"})
	public static void raju() {
	     System.out.println("raju");
	}
	@Test(priority=3)
	public static void Paractice() {
	     System.out.println("it is with priority 3");
	}
	@Test(parameters={"abc"})
	public static void Fai() {
	     System.out.println("it is with........");
	     Assert.assertEquals(false, true);
	}
	@Test
	public static void xmltest(){
		 System.out.println("xml test will run automate");
	}
}
