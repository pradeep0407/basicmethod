package webdriver;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
@Listeners(webdriver.Runtestng.class)
public class Testnrun extends Liserner{

	
	@BeforeTest(groups="sdd")
	public static void abc(){
		System.out.println("test ng running 1 method ");
	}
	@BeforeTest
	public static void wbc(){
		System.out.println("test ng running 2 method ");
	}
	@Test
	public static void mbc(){
		System.out.println("test ng running 3 method ");
	}
	@BeforeTest(groups="sdd")
	public static void nbc(){
		System.out.println("test ng running 4 method ");
	}
	@AfterTest
	public static void bbc(){
		System.out.println("test ng running 5 method ");
	}
	@AfterTest(groups="sdd")
	public static void cbc(){
		System.out.println("test ng running 6 method ");
	}
	@AfterTest
	public static void dbc(){
		System.out.println("test ng running 7 method ");
	}
	@Test
	public static void ebc(){
		System.out.println("test ng running 8 method ");
	}
	@Test
	public static void fbc(){
		System.out.println("test ng running 9 method ");
	}
	
}
