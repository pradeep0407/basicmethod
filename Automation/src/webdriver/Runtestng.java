package webdriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

@Listeners(webdriver.Runtestng.class)

public class Runtestng extends Liserner{
     
	@Test(groups="Regression")
	public static void testng1(){
		System.out.println("testng1");
	}
	@Test
	public static void testng2(){
		System.out.println("testng2");
	}
	@Test(groups="Regression")
	public static void testng3(){
		System.out.println("testng3");
	}
	@Test (parameters = {"xyz"})
	public static void testng4(int abs ){
		System.out.println("testngs");
	}
	@Test(groups="Raj")
	public static void testng5(){
		System.out.println("testng5");
	}
	
	@Test
	public static void testng6(){
		System.out.println("testng6");
	}
	@Test(groups="Raj")
	public static void testng7(){
		System.out.println("testng7");
	}
	
	@Parameters({ "browser"})
	@Test 
	 public void getFirefox(){
	                //System.setProperty("webdriver.gecko.driver", "geckodriver.exe path");
	 System.setProperty("webdriver.gecko.driver", "driver//geckodriver.exe");
	                System.out.println("GetFirefox Method is running on Thread : " + Thread.currentThread().getId());
	                FirefoxDriver driver = new FirefoxDriver();
	 driver.get("http://www.SoftwareTestingMaterial.com");
	 driver.close();
	 }
	 @Test
	 public void getChorme(){
	                //System.setProperty("webdriver.chrome.driver", "chromedriver.exe path");
	 System.setProperty("webdriver.chrome.driver", "driver//chromedriver.exe");
	                System.out.println("GetChrome Method is running on Thread : " + Thread.currentThread().getId());
	                ChromeDriver driver = new ChromeDriver();
	 driver.get("http://www.SoftwareTestingMaterial.com");
	 driver.close();
	 }
	 
	 @Test
	 public void getie(){
	                
	                System.out.println("ie Method is running on Thread : " + Thread.currentThread().getId());
	               System.out.println("i m getting without");
	 }
}
